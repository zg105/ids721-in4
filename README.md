# ids721-in4



## Getting started
1. Create function 1 by running `cargo lambda new func1`, this function simply convert all lowercase letters to uppercase letters.
2. Create function 2 by running `cargo lambda new func2`, this function removes numbers, whitespaces and other signals from payload.
3. Implement and test:
![](pic/1.png) \
![](pic/2.png)
4. Build and Deploy lambda function:
![](pic/3.png) \
![](pic/4.png)
5. Create a step function
![](pic/5.png)
6. Run test
![](pic/6.png)

