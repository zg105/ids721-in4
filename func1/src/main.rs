// Import necessary modules
use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

// Define a struct to represent the input data
#[derive(Deserialize, Serialize)]
struct Input {
    data: String, 
}

// Define a struct to represent the output data
#[derive(Deserialize, Serialize)]
struct Output {
    data: String, 
}

// Main function, entry point of the program
#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(process_data); 
    lambda_runtime::run(func).await?; 
    Ok(())
}

// Function to process the input data
async fn process_data(event: Input, _ctx: Context) -> Result<Output, Error> {
    let data = event.data.to_uppercase(); 
    Ok(Output { data }) 
}